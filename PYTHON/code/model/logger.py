import sys
import logging
import argparse


def set_logging_level(x):
    options = {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL

    }
    option = logging.DEBUG
    if (x in options):
        option = options[x]
    logging.basicConfig(stream=sys.stdout, level=option)


def argumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--outputlevel", "-o",
                        help="Output level: [DEBUG;INFO;WARNING;ERROR;CRITICAL].", type=str, default=None, required=False)
    return parser.parse_args()
