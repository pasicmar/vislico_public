import docx
from docx.shared import Mm
from model.subtitles.srt_loader import SrtFormer
from datetime import timedelta
from model.logger import *

class DocumentCreator():
    def __init__(self, inverted_connections, slides, video_name, pdf_name, subtitles_path, document_path) -> None:
        self.connections=inverted_connections
        self.subtitles_path=subtitles_path
        self.slides = slides
        self.pdf_name = pdf_name
        self.video_name = video_name
        self.document_path=document_path

    def create_docs_file(self):
        data = self.connections
        
            
        srt_former = SrtFormer(self.subtitles_path)
        srt_former.loadSublitles()
        try:
            doc = docx.Document()
            doc.add_heading('generated text from %s for %s.pdf ' % (
                self.video_name, self.pdf_name), 0)
            current_time = timedelta(seconds=0, minutes=0, hours=0)
            image=None
            if(data==[] or data == None):
                paragraph_text = srt_former.get_all_text()
                doc.add_paragraph(paragraph_text)
            else:
                for n in data:
                    if(image!=n[1]):
                        doc.add_picture(n[1], width=Mm(150))
                        image=n[1]
                    time_tuple=n[0]
                    time_format = timedelta(
                        seconds=time_tuple[2], minutes=time_tuple[1], hours=time_tuple[0])
                    paragraph_text = srt_former.get_text(current_time, time_format)
                    doc.add_paragraph(paragraph_text)
                    current_time = time_format
            doc.save(self.document_path)
        except IOError:
           logging.debug('docunent wasnt created due tu error')

    