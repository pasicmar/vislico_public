import cv2


class VideoToFramesGenerator:
        
        def __init__(self, video_path, output_path, timing) -> None:
                self.video = cv2.VideoCapture(video_path)  
                self.output_path=output_path
                self.timing=timing

        def videoConvertor(self):
                pass

class CV2VideoFramesGenerator(VideoToFramesGenerator):
        
        # generate video frame  
        def videoConvertor(self):
                video = self.video
                
                fps = video.get(cv2.CAP_PROP_FPS)
                count = 0
                currentFPS=count*self.timing*fps
                video.set(cv2.CAP_PROP_POS_FRAMES,(currentFPS))
                success,image = video.read()
                while success:
                        currentMicroSec=count*self.timing*1000
                        seconds=currentMicroSec//1000
                        minutes=seconds//60
                        hours=minutes//60  
                        seconds=seconds%60
                        minutes=minutes%60
                        hours=hours%24
                        
                        if not cv2.imwrite(r'%s/%d_%d_%d.jpg' % (self.output_path, hours, minutes, seconds), image):
                                raise Exception("Could not write image")   
                        
                        count = count + 1  
                        currentFPS=count*self.timing*fps
                        video.set(cv2.CAP_PROP_POS_FRAMES,(currentFPS))  
                        success,image = video.read()

