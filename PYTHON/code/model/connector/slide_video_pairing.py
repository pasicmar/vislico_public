import pycountry
import cv2
import gc
import difflib
import multiprocessing
import json
import os
from tokenize import Double, String
from model.logger import *
import traceback

# text simularity block
module_pytesseract_loaded = False
module_easyocr_loaded = False
try:
    from pytesseract import pytesseract
    module_pytesseract_loaded = True
    path_to_tesseract = r'tesseract_files/tesseract.exe'
except:
    module_pytesseract_loaded = False

try:
    import easyocr
    module_easyocr_loaded = True
except:
    module_easyocr_loaded = False


def make_sentence(result):
    return ' '.join([r[1] for r in result])


def count_similarity(text1, text2):
    return difflib.SequenceMatcher(a=text1.lower(), b=text2.lower()).ratio()


def find_pairing(frame, slides):
    frame_text = image_to_text(frame)
    slides_for_frame = []
    for slide in slides:
        slide_text = slide.text_of_slide
        similarity = count_similarity(frame_text, slide_text)
        slides_for_frame.append((similarity, slide))
    sim, slide = max(slides_for_frame, key=lambda x: x[0])

    if sim < SIMILARITY_BOUND:
        return None
    return slide


def image_to_text_pytesseract(path_to_image):
    pytesseract.tesseract_cmd = path_to_tesseract
    text_from_image = pytesseract.image_to_string(path_to_image, lang='ces')
    return text_from_image


def image_to_text(path_to_image):
    return image_to_text_pytesseract(path_to_image)


def image_to_text_easyocr(path_to_image):
    text_from_image = make_sentence(reader.readtext(path_to_image))
    return text_from_image


# visual simmularity block
def scale_down(img, scale):
    width = img.shape[1]
    height = img.shape[0]
    dim = (int(width/scale), int(height/scale))
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    del img
    return resized


def resize(img, expectWidth):
    width = img.shape[1]
    height = img.shape[0]
    perc = expectWidth/width
    expectedHeight = int(height*perc)
    dim = (expectWidth, expectedHeight)
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return resized


def count_histogram(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    colors = ('b', 'g', 'r')
    hist_values = {}
    for i, color in enumerate(colors):
        hist = cv2.calcHist([img], [i], None, [256], [0, 256])
        hist_values[color] = hist
    return hist_values


def compare_histograms(hist1, hist2):
    colors = ('b', 'g', 'r')
    value = 1
    for i, color in enumerate(colors):
        val = cv2.compareHist(hist1[color], hist2[color], cv2.HISTCMP_CORREL)
        value *= val
    return value


def generate_tested_widths(frames, slides, size):
    frame = frames[0]
    slide = slides[0]
    img_frame = cv2.imread(frame)
    img_slide = cv2.imread(slide)

    f_width = img_frame.shape[1]
    f_height = img_frame.shape[0]

    s_width = img_slide.shape[1]
    s_height = img_slide.shape[0]
    del img_frame
    del img_slide
    widths = []
    for i in range(1, size+1):
        w_tested = i*(f_width/size)
        ratio = w_tested/s_width
        h_tested = int(s_height*ratio)
        if h_tested > f_height:
            break
        widths.append(int(w_tested))

    ratio = f_width/s_width
    h_tested = int(s_height*ratio)
    if h_tested > f_height:
        ratio = f_height/s_height
        w_tested = int(s_width*ratio)
        widths.append(int(w_tested))
    else:
        widths.append(int(f_width))

    return widths

# stores connections data
class ChangedSlide(object):
    def __init__(self, img, width, path) -> None:
        self.width = width
        self.path = path
        self.img = resize(img, width)
        self.histogram = count_histogram(img)


class Slide(object):

    def __init__(self, path, widths) -> None:
        self.slide = path
        self.image = scale_down(cv2.imread(path), SCALED_DOWN)
        self.widths = widths
        self.slideVersions = [ChangedSlide(
            self.image, width, path.split('.')[0]) for width in widths]
        self.text_of_slide = image_to_text(path)

    def try_histogram_match(self, img):
        histogram = -1
        for version in self.slideVersions:
            slide_img = version.img

            result = cv2.matchTemplate(slide_img, img, method)
            _, _, mnLoc, _ = cv2.minMaxLoc(result)
            MPx, MPy = mnLoc
            trows, tcols = slide_img.shape[:2]
            match = img[MPy:MPy+trows, MPx:MPx+tcols]
            
            histogram = compare_histograms(
                count_histogram(match), version.histogram)
            if (histogram < HISTOGRAM_BOUNDARY):
                continue
            return True
        return False

# main algoruthm. Things are global becouse of paralell running
def global_data_setting(language_code_get):
    global SIZE
    global HISTOGRAM_BOUNDARY
    global SCALED_DOWN
    global SIMILARITY_BOUND

    global method
    global reader
    global path_to_tesseract
    global language_code
    language_code = language_code_get

    global image_to_text_method


    SIZE = 3
    HISTOGRAM_BOUNDARY = 0.15
    SIMILARITY_BOUND = 0.30
    SCALED_DOWN = 1

    method = cv2.TM_SQDIFF_NORMED

    if (module_pytesseract_loaded):
        image_to_text_method = image_to_text_pytesseract
    else:
        if (module_easyocr_loaded):
            reader = easyocr.Reader([language_code], gpu=True)
            image_to_text_method = image_to_text_easyocr
        else:
            def image_to_text_method(_): return 0.0


def connect_frames_with_slides(frames, slides, language_code):
    global_data_setting(language_code)

    width_to_test = generate_tested_widths(frames, slides, SIZE)
    width_to_test = [int(w/SCALED_DOWN)for w in width_to_test]
    parsedSlides = [Slide(slide, width_to_test) for slide in slides]
    results = {}
    for frame in frames:
        gc.collect()
        slide = get_most_similar(parsedSlides, frame)
        results[frame] = slide
    return results


def get_most_similar(slides, frame):
    paired = find_pairing(frame, slides)
    if (paired == None):
        return None
    frame_image = scale_down(cv2.imread(frame), SCALED_DOWN)
    match = paired.try_histogram_match(frame_image)
    del frame_image

    if (paired == None):
        return None

    return paired.slide

# paralel running
class VideoSlideConnector:

    def __init__(self, frames, slides, path):
        self.frames = frames
        self.slides = slides
        self.path = path
        self.proces = []

    def is_ready(self):
        if (len(self.proces) == 0):
            return False
        for p in self.proces:
            if (not p.ready()):
                return False
        return True

    def store_data(self):
        path = self.path
        dictionary_data = {}
        one_succesfull = False
        for fs in self.files:
            try:
                f = open(fs, "r")
                dictionary_data.update(json.loads(f.read()))
                one_succesfull = True
                os.remove(fs)
            except:
                logging.error("One of paralell connection proccess failed.")
        if (not one_succesfull):
            logging.error("Connection totally failed.") 

        all_connections = {k: [] for k in self.slides}
        for k, v in dictionary_data.items():
            if (v != None):
                time = k.split('/')[-1].split('\\')[-1]
                all_connections[v].append(time)

        all_connections = {k: v for k,
                           v in all_connections.items() if len(v) > 0}

        data = json.dumps(all_connections)
        f = open(path, "w")
        f.write(data)
        f.close()
        return all_connections

    def connect(self, language_code):
        PROC_NUM = 1
        global module_pytesseract_loaded
        global module_easyocr_loaded
        try:
            multiprocessing.set_start_method('spawn')
        except RuntimeError:
            pass
        parts = [(self.slides, self.frames[i::PROC_NUM])
                 for i in range(PROC_NUM)]
        self.pool = multiprocessing.Pool(PROC_NUM)
        pool = self.pool
        self.proces = []
        proc = 0
        self.files = []
        for data in parts:
            file = "tmp_"+str(proc)+".json"
            proc += 1
            self.files.append(file)
            serialized = json.dumps(data)
            self.proces.append(pool.apply_async(
                VideoSlideConnector.connectTogether, (serialized, file, language_code)))

    def connectTogether(serialized, file, language_code=None):
        slides, frames = json.loads(serialized)
        if (not language_code):
            language_code = 'cs'

        try:
            data = json.dumps(connect_frames_with_slides(
                frames, slides, language_code))
        except Exception as e:
            data = "THIS FAILED "+str(e)+" STACKTRACE " + \
                str(traceback.format_exc())
        f = open(file, "w")
        f.write(data)
        f.close()


# returns all avaluable languages
def get_pytesseract_languages():
    pytesseract.tesseract_cmd = path_to_tesseract
    language_list = pytesseract.get_languages(config='')
    all_languages = {}
    all_languages['Czech'] = 'ces'
    all_languages['English'] = 'eng'
    for language in language_list:
        language_name = pycountry.languages.get(alpha_3=language)
        if (language_name == None):
            continue
        all_languages[language_name.name] = language_name.alpha_3
    return all_languages
    
        
def get_avaluable_languages():
        pytesseract.tesseract_cmd = path_to_tesseract
        codes = get_pytesseract_languages()
        codes = {v: k for k, v in codes.items()}
        return codes