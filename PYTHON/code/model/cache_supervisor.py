import os
from slugify import slugify
import unicodedata
import re
from os import walk
from os import listdir
from os.path import isfile, join
from pathlib import Path
from model.logger import *


class CacheSupervisor():
    def __init__(self) -> None:
        self.subtitles_file_pathes = []
        self.pdf_path = None
        self.slides_folder = None
        self.pdf_file_name = None
        self.presentation_folder = None
        self.conections_file_path = None
        self.video_path = None
        self.video_file_name = None
        self.folder_name = None
        self.frames_folder = None
        self.default_srt_path = None

# return true if presentation was already used
    def add_presentation(self, pdf_path):
        self.pdf_path = os.path.normpath(pdf_path)
        self.pdf_file_name = self.get_valid_filename(
            self.pdf_path.split(os.sep)[-1].split('.')[:-1])
        self.presentation_folder = '%s/%s' % (
            self.folder_name, self.pdf_file_name)
        self.slides_folder = '%s/slides' % self.presentation_folder
        self.create_conections_path()
        logging.info('self.conections_file_path' + self.conections_file_path)
        if self.add_folder(self.presentation_folder):
            logging.info('presentation folder existed before')
            return True
        else:
            self.add_folder(self.slides_folder)
            logging.info('presentation folder didnt exist before')
            return False

# return true if video was already used
    def add_video(self, video_path, seconds):
        self.video_path = os.path.normpath(video_path)
        self.video_file_name = self.get_valid_filename(
            self.video_path.split(os.sep)[-1])
        self.folder_name = 'cache/%s_%s' % (self.video_file_name, seconds)
        self.frames_folder = '%s/frames' % self.folder_name
        self.default_srt_path = '%s/generic.srt' % self.folder_name
        if (self.add_folder(self.folder_name)):
            logging.info('lets check video existance out')
            self.check_old_pathes()
            return True
        self.add_folder(self.frames_folder)
        return False

    def create_srt_path(self, language):
        subtitles_file_path = '%s/%s.srt' % (
            self.folder_name, language)
        self.subtitles_file_pathes.append(subtitles_file_path)
        return subtitles_file_path

    def create_document_path(self, lang_code):
        downloads_path = str(Path.home() / "Downloads")
        document_name = '%s/%s_%s_%s.docx' % (downloads_path,
                                              self.video_file_name, self.pdf_file_name, str(lang_code))
        if os.path.exists(document_name):
            try:
                os.remove(document_name)
            except:
                return None
        return document_name

# returns content(folders or files) from director
    def get_folder_folders(self, path):
        f = []
        for (a, folders, files) in walk(path):
            f.extend(folders)
            break
        return f

    def get_folder_files(self, path):
        f = []
        for (a, folders, files) in walk(path):
            f.extend(files)
            break
        return f

# controls if this video was used before and set old pathes if yes
    def check_old_pathes(self):
        f = self.get_folder_folders(self.folder_name)
        if (len(f) > 0):
            self.set_old_pathes(f)

    def set_old_pathes(self, f):
        for folder in f:
            if (folder == 'frames'):
                self.frames_folder = '%s/frames' % self.folder_name
            else:
                self.pdf_file_name = folder
                self.presentation_folder = '%s/%s' % (self.folder_name, folder)
                logging.info(
                    'set self.presentation_folder from cache' + self.presentation_folder)

                presentation_folders = self.get_folder_folders(
                    self.presentation_folder)
                for folder in presentation_folders:
                    if (folder == 'slides'):
                        self.slides_folder = '%s/%s' % (
                            self.presentation_folder, folder)
                        logging.info(
                            'set self.slides_folder from cache'+self.slides_folder)

                presentation_files = self.get_folder_files(
                    self.presentation_folder)
                for file in presentation_files:
                    if (file == 'connection.json'):
                        self.conections_file_path = '%s/connection.json' % (
                            self.presentation_folder)
                        logging.info('set self.conections_file_path from cache' +
                                     self.conections_file_path)
                if (presentation_files == []):
                    self.create_conections_path()
        video_srt_files = self.get_folder_files(self.folder_name)
        for file in video_srt_files:
            self.subtitles_file_pathes.append(file)
            if (file == 'generic.srt'):
                self.default_srt_path = '%s/generic.srt' % self.folder_name

    # returns true if folder existed before
    def add_folder(self, folder_path):
        logging.debug('check existance ' + folder_path)
        if not os.path.exists(folder_path):
            logging.info('lets create it' + folder_path)
            os.makedirs(folder_path)
            return False
        else:
            return True

    # check_if_folder_empty
    def check_if_slides_folder_not_empty(self):
        if (not self.presentation_folder):
            return False
        if len(os.listdir(self.presentation_folder+'/slides')) > 0:
            return True
        else:
            return False

    def create_conections_path(self):
        logging.debug("lets create_conections_path")
        if (not self.presentation_folder):
            logging.debug('not')
            return False
        self.conections_file_path = '%s/connection.json' % (
            self.presentation_folder)
        logging.debug("lets create_conections_path" +
                      self.conections_file_path)
        return True

# return true if file exist
    def chech_connections_existance(self):
        logging.debug(" lets chech_connections_existance ")
        if (not self.presentation_folder):
            logging.debug('not')
            return False
        path = Path('%s/connection.json' % (self.presentation_folder))
        logging.debug(" chech_connections_existance " +
                      str(path.is_file()))
        return path.is_file()

    def chech_existance(self, language):
        if (not self.folder_name):
            logging.info('chech language existance return no such folder')
            return None
        path = Path('%s/%s.srt' % (self.folder_name, language))
        if path.is_file():
            logging.info(
                'chech language existance return there is such folder')
            return path
        return None

    def get_valid_filename(self, value, allow_unicode=False):
        value = str(value)
        if allow_unicode:
            value = unicodedata.normalize('NFKC', value)
        else:
            value = unicodedata.normalize('NFKD', value).encode(
                'ascii', 'ignore').decode('ascii')
        value = re.sub(r'[^\w\s-]', '', value.lower())
        return re.sub(r'[-\s]+', '-', value).strip('-_')
