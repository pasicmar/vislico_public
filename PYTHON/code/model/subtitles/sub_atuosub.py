import os
import moviepy.editor
from moviepy import *
from view.select_window import *
from model.logger import *
from model.subtitles.supported_languages import supported_languages
import autosub
import shutil



class SubtitlesGenerator:
    def __init__(self, video_path, output_file) -> None:
        self.video_path = video_path
        self.output = output_file
        self.thread_is_done = False
        
    def generate_subtitles():
        pass
    
    #wait for the subtitles
    def waitSubtitles():
        pass 

class Autosub(SubtitlesGenerator):
    
    def autosub_generate(self,video_path,output,language_code):
        tmp_file=str(os.getpid())+'audio.wav'

        video = moviepy.editor.VideoFileClip(video_path)
        audio = video.audio
        #replace the parameter with the location along with filename
        audio.write_audiofile(tmp_file)
        try:
            autosub.generate_subtitles(tmp_file,output=output,src_language=language_code,dst_language=language_code,subtitle_file_format='srt')
            logging.info("autosub generating went GOOD")
            #copy file
        except Exception as e :
            logging.error("autosub generating FAILED")
            with open(output,'w') as selected:
                selected.write("""
1
00:00:00,000 --> 99:00:00,000
---- GENERATING OF SUBTITLES FAILED ----"""+str(e))


        try :
            target_lang_file = output.replace("generic.srt", str(language_code)+".srt")
            shutil.copyfile(output,target_lang_file)
            logging.info("Subtitles stored for targed language.")
        except:
            logging.info("Storing subtitles as target language failed.")

        os.remove(tmp_file)

        self.thread_is_done = True
    
    def subtitlesReady(self):
        return self.thread_is_done
        
    def waitSubtitles(self):
        self.pool.join()


    def generate_subtitles(self,language_code):
        logging.info('generate_subtitles'+language_code)
        from threading import Thread
        print("done")
        self.thread_is_done = False
        self.srt = Thread(target = Autosub.autosub_generate, args =(self,self.video_path,self.output,language_code))
        self.srt.start()

def get_video_avaluable_languages():
    return supported_languages


