import srt
from googletrans import Translator
from model.logger import *
import shutil
from os.path import exists
from os import remove

class SrtFormer():

    def __init__(self, default_subtitles_adress) -> None:
        self.subtitles_adress = default_subtitles_adress
        self.translator = Translator(service_urls=['translate.googleapis.com'])
        self.isLoaded = False
        
    def subtitle_file_exist(self):
        try:
            return exists(self.subtitles_adress)
        except:
            return False
        

    def loadSublitles(self):
        if(self.isLoaded):
            return
        subtitles = open(self.subtitles_adress, "r", encoding="UTF-8")
        self.data = subtitles.read()
        self.parsed = list(srt.parse(self.data))
        self.isLoaded = True

 # returns all linnes contening input
    def search(self, text):
        return [sub for sub in self.parsed if (text.lower() in sub.content.lower())]

# returns start and content of the currant word usage
    def get_time_of_usage(self, text):
        subs = self.search(text)
        result = {}
        for sub in subs:
            result[sub.start] = sub.content
        return result


# returns text only from the srt during current time
    def get_text(self, start, end):
        text = ''
        for sub in self.parsed:
            if (sub.start >= start and sub.end <= end):
                text += sub.content+"  "
        return text

    def get_all_text(self):
        text = ''
        for sub in self.parsed:
            text += sub.content+"  "
        return text

    def get_current_text(self, time):
        logging.debug(time/1000)
        for sub in self.parsed:
            if ((sub.start.total_seconds()) <= time/1000 <= (sub.end.total_seconds())):
                return (sub.content)

    def translater(self, text, target_language):
        self.translator.translate(
            text, dest=target_language)

    def translate_srt(self, target_language, target_address):
        tmf_file_name = target_address+"_tmp"
        result = list(srt.parse(self.data))
        f = open(tmf_file_name, "w", encoding="UTF-8")
        for text in self.parsed:
            translation = self.translator.translate(
                text.content, dest=target_language)
            result[text.index-1].content = translation.text
        f.write(srt.compose(result))
        f.close()
        shutil.copyfile(tmf_file_name, target_address)
        remove(tmf_file_name)
        print("File copied successfully.")
