from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QVBoxLayout, QWidget, QComboBox,QDialog

class select_window(QDialog):
    def __init__(self,label,list_of_options,default=None):
        super().__init__()
        self.list_of_options = list_of_options

        self.layout = QVBoxLayout(self)
        self.setLayout(self.layout)
        
        self.label = QLabel(label)
        self.layout.addWidget(self.label)
        
        self.combo = QComboBox(self)
        self.layout.addWidget(self.combo)
        self.combo.currentTextChanged.connect(self.change_selection)
        self.combo.addItems(list_of_options)
        
        if default == None:
            default = 0
        self.selected = default

        self.confirm_selection = QPushButton("OK")
        self.layout.addWidget(self.confirm_selection)
        self.confirm_selection.clicked.connect(self.confirmed)
        
    def change_selection(self, value):
        self.selected = self.list_of_options[value]
        
    def confirmed(self):
        self.done(self.selected)  
        
    def select_from_list(label,list_of_options,default=None):
        val_number = {}
        key_value = {}
        index = 0
        
        for k,v in list_of_options.items():
            val_number[v] = index
            key_value[index] = k 
            index += 1
        
        selection_list = select_window(label,val_number,default)
        result = selection_list.exec()
        
        if(result not in key_value):
            return key_value[result]
        return key_value[result]     
