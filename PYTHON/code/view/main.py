import sys
from view.main_window import *
from tkinter import filedialog
from PyQt5 import QtCore, QtGui, QtWidgets, QtMultimedia
from PySide2.QtMultimedia import QMediaPlayer
import mimetypes
import multiprocessing
from model.logger import *

from controller.Controller import *


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowTitle('ViSliCo')
        self.setWindowIcon(QtGui.QIcon(':/icons/icons/gallery.png'))
        self.ui.restore_or_maximize.clicked.connect(
            self.restore_or_maximize_window)
        self.ui.close.clicked.connect(lambda: self.close())
        self.ui.header_frame.mouseMoveEvent = self.moveWindow
        self.ui.pushButton_6.clicked.connect(self.slideLeftMenu)
        self.set_start_window()
        self.selected_language_code = None
        self.translated_waiting = []

        # video
        self.ui.select_video_button.clicked.connect(self.upload_video)
        self.ui.play_btn.clicked.connect(self.play_video)
        self.ui.volumeSlider.valueChanged[int].connect(self.changeValue)

        # presentation

        self.ui.select_presentation_button.setEnabled(False)
        self.ui.select_presentation_button.clicked.connect(
            self.upload_presentation)

        # subtitles
        self.ui.on_subtitless_button.setVisible(False)
        self.ui.subtitles_language_button.setVisible(False)
        self.ui.on_subtitless_button.clicked.connect(
            self.switch_subtitles)
        self.ui.subtitles_language_button.clicked.connect(
            self.switch_srt_language)

        # searching
        self.ui.search_btn.clicked.connect(self.get_text_times_of_usage)

        # generate document
        self.ui.generate_document_button.setEnabled(False)
        self.ui.generate_document_button.clicked.connect(
            self.generate_document)

        # speed
        self.ui.speed_down_btn.clicked.connect(lambda: self.change_speed(-1))
        self.ui.speed_up_btn.clicked.connect(lambda: self.change_speed(1))

        # change
        self.ui.change_video_btn.setVisible(False)
        self.ui.change_presentation_btn.setVisible(False)
        self.ui.change_presentation_btn.clicked.connect(
            self.change_presentation)
        self.ui.change_video_btn.clicked.connect(self.change_video)

    def set_start_window(self):
        self.ui.start_frame.setMaximumWidth(500)
        self.ui.player_controller_frame.setMaximumWidth(0)
        self.ui.player_controller_frame.setEnabled(False)
        self.set_disabled_text_usage()
        self.ui.change_presentation_btn.setEnabled(False)

        self.ui.start_presentation_frame.setMaximumWidth(500)
        self.ui.tabWidget.setMaximumWidth(0)
        
    def set_disabled_text_usage(self):
        self.ui.search_btn.setEnabled(False)
        self.ui.generate_document_button.setEnabled(False)

    def restore_or_maximize_window(self):
        if self.isMaximized():
            self.showNormal()
            self.ui.restore_or_maximize.setIcon(
                QtGui.QIcon(u":/icons/icons/diagonal-arrow.png"))
        else:
            self.showMaximized()
            self.ui.restore_or_maximize.setIcon(
                QtGui.QIcon(u":/icons/icons/exit-full-screen.png"))

    def moveWindow(self, e):
        if self.isMaximized() == False:
            if e.buttons() == Qt.LeftButton:
                self.move(self.pos()+e.globalPos() - self.clickPosition)
                self.clickPosition = e.globalPos()
                e.accept()

    def mousePressEvent(self, event):
        self.clickPosition = event.globalPos()

    def slideLeftMenu(self):
        width = self.ui.slide_menu.width()
        if (width == 0):
            newWidth = 600
            self.ui.pushButton_6.setIcon(
                QtGui.QIcon(u":/icons/icons/chevron.png"))
        else:
            newWidth = 0
            self.ui.pushButton_6.setIcon(
                QtGui.QIcon(u":/icons/icons/side_menu.svg"))

        self.animation = QPropertyAnimation(
            self.ui.slide_menu, b'maximumWidth')
        self.animation.setDuration(250)
        self.animation.setStartValue(width)
        self.animation.setEndValue(newWidth)
        self.animation.setEasingCurve(QtCore.QEasingCurve.InOutQuart)
        self.animation.start()

    def play_video(self):
        if (self.ui.mediaPlayer.state() == QMediaPlayer.PlayingState):
            self.ui.mediaPlayer.pause()
            self.ui.play_btn.setIcon(
                QtGui.QIcon(u":/icons/icons/play-button.png"))
        else:
            self.ui.mediaPlayer.play()
            self.ui.play_btn.setIcon(
                QtGui.QIcon(u":/icons/icons/pause.svg"))

    def position_changed(self, position):
        converted = self.convert_milliseconds_to_time(position)
        self.ui.timeNow.setText(('%s:%s:%s') % (
            converted[0], converted[1], converted[2]))
        self.ui.timeSlider.setValue(position)
        if not (self.controller.subtitle_path == None):
            self.ui.subtitles_language_button.setVisible(True)
            self.ui.on_subtitless_button.setVisible(True)
            out = self.controller.get_current_subtitles(position)
            self.ui.titles_label.setText(out)

    # set total vdeo duration
    def set_duration(self, duration):
        self.ui.vieoplayer_frame.setMaximumWidth(self.ui.videoplayer.width())
        self.ui.timeSlider.setRange(0, duration)
        converted = self.convert_milliseconds_to_time(duration)
        self.ui.timeAll.setText(('%s:%s:%s') % (
            converted[0], converted[1], converted[2]))

    def set_position(self, position):
        self.ui.mediaPlayer.setPosition(position)

    def handle_error(self):
        return 
        self.ui.playButton.setEnabled(False)
        logging.error('mediaPlayer error')

    def changeValue(self, value):
        if (value == 0):
            self.ui.volumeButton.setIcon(
                QtGui.QIcon(u":/icons/icons/no-sound.png"))
        else:
            self.ui.volumeButton.setIcon(
                QtGui.QIcon(u":/icons/icons/volume.png"))
        self.ui.mediaPlayer.setVolume(value)

    def statusChanged(self, status):
        if status == QMediaPlayer.EndOfMedia:
            self.ui.mediaPlayer.pause()
            self.ui.play_btn.setIcon(
                QtGui.QIcon(u":/icons/icons/play-button.png"))

    def switch_subtitles(self):
        if self.controller.subtitle_path == None:
            logging.info('srt language is not set so we use detected')
            self.controller.get_subtitles('generic')
        self.ui.search_btn.setEnabled(True)
        self.try_enable_document_generation()
        height = self.ui.vieoplayer_frame.height()
        if (height == 0):
            newheight = 45
        else:
            newheight = 0
        self.ui.vieoplayer_frame.setFixedHeight(newheight)

    def set_another_srt_language(self, value):
        logging.info('set_another_srt_language' + value)
        self.ui.srt_languages_Box.setMaximumWidth(0)
        selected_language = value
        if (selected_language == 'autodetected'):
            language_code = 'generic'
        else:
            language_code = [
                i for i in self.language_dictionary if self.language_dictionary[i] == selected_language][0]
        self.selected_language_code = language_code
        self.get_subtitles_for_language(language_code, value)

    def get_subtitles_for_language(self, language_code, language_full):
        logging.debug('get_subtitles_for_language'+language_full)
        if (self.controller.subtitle_exist(language_code)):
            language_path, _ = self.controller.get_source_and_path(
                language_code)
            self.controller.load_subtitles_from_path(language_path)
        else:
            if(language_full not in self.translated_waiting):
                self.translated_waiting.append(language_full)
                self.show_info_messagebox(
                    'Please wait. Data is translated to %s. The result will be displayed in the subtitles' % language_full)
                self.get_subtitles_for_language_create(language_code, None)

    def get_subtitles_for_language_create(self, language_code, process):
        if process == None:
            language_path, default_path = self.controller.get_source_and_path(
                language_code)
            logging.debug("creates language translate threads")
            try:
                multiprocessing.set_start_method('spawn')
            except RuntimeError:
                pass
            pool = multiprocessing.Pool(1)
            controller = Controller(self.controller.video_path)
            process = pool.apply_async(controller.create_and_get_subtitles_path, (language_path, default_path, language_code,))
            pool.close()

        if not process.ready():
            logging.debug('subtitles translation is processing')
            QtCore.QTimer.singleShot(
                3000, lambda: self.get_subtitles_for_language_create(language_code, process))
        else:
            logging.info('subtitles translation is ready')
            path = process.get()
            if(path == None):
                return
            self.controller.load_subtitles_from_path(path)

    def switch_srt_language(self):
        width = self.ui.srt_languages_Box.width()
        if (width == 0):
            new_width = 150
        else:
            new_width = 0
        self.ui.srt_languages_Box.setMaximumWidth(new_width)

    def play_video_from(self, time):
        hours = time//3600000
        minutes = (time//60000) % 60
        seconds = (time//1000) % 60
        self.ui.timeNow.setText(('%s:%s:%s') % (hours, minutes, seconds))
        self.ui.timeSlider.setValue(time)
        self.ui.mediaPlayer.setPosition(time)

    def set_videoplayer_parameters(self):
        self.ui.mediaPlayer = QtMultimedia.QMediaPlayer(self)
        self.ui.mediaPlayer.setVideoOutput(self.ui.widget)
        self.ui.mediaPlayer.mediaStatusChanged.connect(self.statusChanged)
        self.ui.mediaPlayer.positionChanged.connect(self.position_changed)
        self.ui.mediaPlayer.durationChanged.connect(self.set_duration)
        self.ui.mediaPlayer.error.connect(self.handle_error)
        self.ui.timeSlider.sliderMoved.connect(self.set_position)
        self.ui.select_presentation_button.setEnabled(True)
        self.ui.player_controller_frame.setEnabled(True)
        self.ui.change_video_btn.setVisible(True)
        self.ui.on_subtitless_button.setVisible(False)
        self.ui.subtitles_language_button.setVisible(False)
        # self.ui.play_btn.setIcon(
        #     QtGui.QIcon(u":/icons/icons/play-button.png"))

    def remove_presentation(self):
        try:
            self.groupBox.deleteLater()
            self.groupBox = None
            self.ui.start_presentation_frame.setMaximumWidth(500)
            self.ui.tabWidget.setMaximumWidth(0)

            icon = QIcon()
            icon.addFile(u":/icons/icons/pdf-file.png", QSize(), QIcon.Normal, QIcon.Off)
            self.ui.select_presentation_button.setText('Select presentation (in pdf)')           
            self.ui.select_presentation_button.setIcon(icon)
            self.ui.start_frame.setMaximumWidth(0)
            self.ui.slides_scrollArea.setMaximumWidth(500)
            self.ui.generate_document_button.setEnabled(False)
        except Exception as e:
            print(e)

    def upload_video(self, event=None):
        filename = filedialog.askopenfilename()
        if (filename != ''):
            mimestart = mimetypes.guess_type(filename)[0]
            mimestart = mimestart.split('/')[0]
            if (mimestart == "video"):
                self.ui.speed.setText(str(1.0))
                self.set_disabled_text_usage()
                self.remove_presentation()
                self.ui.start_frame.setMaximumWidth(0)
                url = QtCore.QUrl.fromLocalFile(filename)
                self.ui.player_controller_frame.setMaximumWidth(167772)
                self.controller = None
                self.controller = Controller(filename)
                self.checkSubtitles()

                codes=self.controller.get_autosub_avaluable_languages()
                code=None
                if(not codes==None):
                    code=select_window.select_from_list("Language of the video:", codes)
                    print(code)
                self.controller.set_video(code)
                self.language_dictionary = self.controller.get_srt_avaluable_languages()
                

                self.ui.srt_languages_Box.addItems(
                    self.language_dictionary.values())
                self.ui.srt_languages_Box.setCurrentText('autodetected')
                self.ui.srt_languages_Box.currentTextChanged.connect(
                    self.set_another_srt_language)

                self.set_videoplayer_parameters()
                self.ui.mediaPlayer.setMedia(QtMultimedia.QMediaContent(url))
                slides = self.controller.get_slides_from_cache()

                self.try_enable_document_generation()
                self.remove_search_results()
                if slides:
                    self.load_connections_from_cache(slides)
                    

    def remove_search_results(self):
        try:
            self.formLayout2 = QFormLayout()
            groupBox = QGroupBox()
            groupBox.setLayout(self.formLayout2)
            scroll = self.ui.text_scrollArea
            scroll.setWidget(groupBox)
            scroll.setWidgetResizable(True)
        except:
            pass

    def try_enable_document_generation(self):
        #slides_ready = self.controller.get_connections_from_cache() # NOT NEEDED RN
        # This is the place, where we decide, if we want to make document test with slides and subtitles, or just the subtitles are enought. 
        # Right now, subtitles are enough - For cases where we do not have presentations.
        if(self.controller.generic_subtitles_exist()):
            self.ui.generate_document_button.setEnabled(True)
        else:
            self.ui.generate_document_button.setEnabled(False)

    def load_connections_from_cache(self, slides, language_code=None):
        logging.info("load_connections are processing")
        self.ui.select_presentation_button.setText(
            """Please wait. Data are processing...
        (It can take up to 20% of the video duration)
        """
        )
        self.ui.select_presentation_button.setIcon(QIcon())
        is_ready = self.controller.get_connections_from_cache(language_code)
        if (is_ready):
            if not (slides == None):
                self.add_picture(slides)
                self.ui.change_presentation_btn.setVisible(True)
                self.ui.start_presentation_frame.setMaximumWidth(0)
                self.ui.select_presentation_button.setIcon(QIcon())
                self.ui.tabWidget.setMaximumWidth(500)
                logging.info('connections_from_cache are ready ')
                self.try_enable_document_generation()
        else:
            logging.debug('connections_from_cache are not ready. lets wait')
            QtCore.QTimer.singleShot(
                10000, lambda: self.load_connections_from_cache(slides, language_code))

   # check generic subtitles existence
    def checkSubtitles(self):
        if (self.controller.generic_subtitles_exist()):
            self.ui.on_subtitless_button.setVisible(True)
            self.ui.subtitles_language_button.setVisible(True)
            self.ui.search_btn.setEnabled(True)
            self.try_enable_document_generation()
            logging.info('generic subtitles are ready ')
            return
        logging.debug('generic subtitles are processing ')
        QtCore.QTimer.singleShot(3000, self.checkSubtitles)

    def upload_presentation(self, event=None):
        filename = filedialog.askopenfilename()
        if (filename != ''):
            mimestart = mimetypes.guess_type(filename)[0]
            mimestart = mimestart.split('/')[1]
            if (mimestart == 'pdf'):
                self.process_pdf(filename)

    # when user alredy choose pdf file
    def process_pdf(self, filename):
        self.ui.select_presentation_button.setText(
            'Pleace wait. Data are processing')
        self.ui.select_presentation_button.setIcon(QIcon())
        
        codes = self.controller.get_tesseract_avaluable_languages()
        language_code = select_window.select_from_list("Language of the presentation:",codes) #druhy vystup
                
        slides = self.controller.process_pdf(filename)
        self.load_connections_from_cache(slides, language_code)
        # self.connections = self.controller.connections

        self.ui.start_frame.setMaximumWidth(0)
        self.ui.slides_scrollArea.setMaximumWidth(500)

    def convert_time_to_miliseconds(self, time_arr):
        logging.debug('convert_time_to_miliseconds ')
        hours = int(time_arr[0])*3600000
        minutes = int(time_arr[1])*60000
        seconds = int(time_arr[2])*1000
        miliseconds = hours+minutes+seconds
        return (miliseconds)

    def add_picture(self, slides):
        logging.debug('add_pictures ')
        self.ui.change_presentation_btn.setEnabled(True)
        self.ui.start_presentation_frame.setMaximumWidth(0)
        self.ui.tabWidget.setMaximumWidth(500)
        self.ui.connections.setMaximumWidth(500)
        self.ui.slides_scrollArea.setVerticalScrollBarPolicy(
            Qt.ScrollBarAlwaysOn)
        self.formLayout = QFormLayout()
        self.groupBox = QGroupBox()
        for n in slides:
            label2 = QLabel()
            pixmap = QPixmap(n)
            label2.setPixmap(pixmap.scaledToWidth(350))
            self.formLayout.addRow(label2)
            dictionary = self.controller.connections
            if dictionary != None:
                if n in dictionary:
                    [self.add_time_button(time) for time in dictionary[n]]
        self.groupBox.setLayout(self.formLayout)
        scroll = self.ui.slides_scrollArea
        scroll.setWidget(self.groupBox)
        scroll.setWidgetResizable(True)

# returns time buttons to current slide
    def add_time_button(self, time):
        time_label = '%d:%d:%d' % (time[0], time[1], time[2])
        time_value = self.convert_time_to_miliseconds(time)
        def func(): return self.play_video_from(time_value)

        result = self.formLayout
        btn = QPushButton(time_label)

        btn.clicked.connect(func)
        btn.setStyleSheet("background-color : rgb(245, 250, 250)")
        result.addRow(btn)

    def get_text_times_of_usage(self):
        self.ui.tabWidget.setMaximumWidth(500)
        text = self.ui.lineEdit.text()
        results,on_defalt = self.controller.search_text_usage(text,self.selected_language_code)
        if on_defalt: 
            self.show_info_messagebox(
            'The translated subtitles are not ready yet, thus search was done on default language.')
        logging.info('get_text_times_of_usage results '+str(results))
        self.ui.tabWidget.setCurrentWidget(
            self.ui.tabWidget.findChild(QWidget, 'word_search_result'))
        self.formLayout2 = QFormLayout()
        groupBox = QGroupBox()
        for time in results.keys():
            converted = self.convert_milliseconds_to_time(
                time.total_seconds()*1000)
            self.set_text_search_information(results[time], converted)
        groupBox.setLayout(self.formLayout2)
        scroll = self.ui.text_scrollArea
        scroll.setWidget(groupBox)
        scroll.setWidgetResizable(True)

    def set_text_search_information(self, text, time):
        label2 = QLabel()
        label2.setText(text)
        label2.setWordWrap(True)
        label2.setFont(QFont('Arial', 10))
        self.formLayout2.addRow(label2)

        time_value = self.convert_time_to_miliseconds(time)
        def func(): return self.play_video_from(time_value)
        label = '%d:%d:%d' % (time[0], time[1], time[2])
        btn = QPushButton(label)

        btn.clicked.connect(func)
        btn.setStyleSheet("background-color : rgb(245, 250, 250)")
        self.formLayout2.addRow(btn)

    def convert_milliseconds_to_time(self, time):
        hours = time//3600000
        minutes = (time//60000) % 60
        seconds = (time//1000) % 60
        return [hours, minutes, seconds]

    def change_speed(self, koeficient):
        current_speed = float(self.ui.speed.text())
        new_speed = round(current_speed+(koeficient*0.1), 1)
        if (new_speed > 0 and new_speed < 4.4):
            self.ui.mediaPlayer.setPlaybackRate(new_speed)
            self.ui.speed.setText(str(new_speed))

    def generate_document(self):
        path,on_defalt = self.controller.generate_document(self.selected_language_code)
        
        if path == None: 
            self.show_info_messagebox(
            'Document is NOT generated! (Please verify, that old document is deleted!)')
        else:
            msg = ""
            if on_defalt: 
                msg = "The source language was used default."

            self.show_info_messagebox(
                'Document is generated and saved into your downloads folder. %s'%msg)

    def change_video(self):
        self.translated_waiting = []
        self.ui.mediaPlayer.stop()
        self.upload_video()

    def change_presentation(self):
        try:
            self.groupBox.deleteLater()
            self.groupBox = None
        except:
            pass
        self.ui.start_presentation_frame.setMaximumWidth(500)
        self.ui.tabWidget.setMaximumWidth(0)
        self.upload_presentation()

    def show_info_messagebox(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(text)
        msg.setWindowTitle("Information MessageBox")
        msg.setStandardButtons(QMessageBox.Ok)
        retval = msg.exec_()