# ViSliCo
ViSliCo (Video Slides Connecotor) je program na spojovaní multimediálních dat jako jsou video a prezentace, obsahující funkcionalitu na prací s nimi. 

## Distribuce
V tomto repozitáři jsou umístěny dvě varianty mého programu ViSliCo: EXE a PYTHON. 

## Instalace programu
Tento návod je pro operační systém windows a může se lišit pro ostatní systémy. Pro usnadnění práce s našim programem, je v této složce přiloženo i instruktážní video[ViSliCo.mp4] a uživatelský manuál[Uzivatelsky_manual.pdf].

### EXE
Pro zprovoznění varianty přes spustitelný soubor exe stačí stáhnout složku EXE a spouštět aplikaci přes soubor ViSliCO.exe, na který je možné si vytvořit zástupce a umístit jej například na plochu.
Při používání programu mohou nastat potíže, které jsou způsobeny chybějícímy knihovnami ve vašem počítači. Pro odstranění těchto potíží je aktuálně možné podívat se na chybovou hlášku při nečekaném ukončení programu a nainstalovat si potřebnou knihovnu. Knihovnu si klidně můžete nainstalovat dopředu, před prvním spuštěním programu, případně ověřit její přítomnost pomocí příkazové řádky.
Odkazy na stažení a případně potřebné informace můžete naleznout níže, v sekci "základní chybějící balíčky".

### PYTHON
Pro variantu v pythonu, která není brána jako hlavní verze distribuce je v repozitáři přiložen soubor requirements.txt.
Takže pokud by jste chtěli spustit program přes python, je potřeba zadat do terminálu 'pip install -r requirements.txt' pro stažení veškerých potřebných knihoven a pak program spustit pomocí pythonu ze souboru ViSliCO.py.


## Základní chybějící balíčky:

Základní chybějící balíčky, které je potřeba stáhnout a nainstalovat pokud je ještě nemáte

FFMPEG 
(Pokud ho nemáte, tak vám nepojede generování titulků.)
odkaz ke stazeni: https://ffmpeg.org/download.html 
Videotutorial na jeho instalaci na windows: https://www.youtube.com/watch?v=22vmzTs5BoE 
Po instalaci je potřeba vymazat složku cache.

POPPLER 
(Pokud ho nemáte tak po přidání pdf filu Vám program vyhodí chybu 'pdf2image.exception')
odkaz ke stazeni: https://blog.alivate.com.au/poppler-windows/ 
Instaluje se podobně jako FFMPEG. Detajlnějši instrukce se dá najít tady https://stackoverflow.com/questions/18381713/how-to-install-poppler-on-windows 
Po instalaci je potřeba vymazat složku cache.

KLite-Codec 
(Pokud ho nemáte tak vám nebude fungovat videoplayer)
odkaz ke stazeni: https://fileforum.com/download/KLite-Codec-Pack_Standard/1094057842/2 
Vyberte si nejvíce základní nastavení. 

Taky by jste se mohli setkat s chybovou hláškou na místě vygenerovaných titulek. V dané situaci je potřeba vejít do složky cache, najít složku se jménem daného videa a smazat soubory .src
